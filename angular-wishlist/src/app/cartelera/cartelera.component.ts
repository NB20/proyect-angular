import { Component, OnInit, Input, Output,EventEmitter } from '@angular/core';
import { Restaurant } from '../models/restaurant.model';

@Component({
  selector: 'app-cartelera',
  templateUrl: './cartelera.component.html',
  styleUrls: ['./cartelera.component.css']
})
export class CarteleraComponent implements OnInit {
@Input() img:string[];
destino: Restaurant[];
@Output() cambiarPadre = new EventEmitter<string>();

  constructor() {

   }

  ngOnInit(): void {
  }

  cambio(url:string):boolean {
    this.destino.push(new Restaurant(url, url, ""))
    return false
  }

}

