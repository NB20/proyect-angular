import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Restaurant } from '../models/restaurant.model';
import { RestaurantApiClient } from '../models/restaurant-api-client.model';

@Component({
  selector: 'app-lista',
  templateUrl: './lista.component.html',
  styleUrls: ['./lista.component.css']
})
export class ListaComponent implements OnInit {
  destinos: Restaurant[];
  img1: string = 'https://cdn.pixabay.com/photo/2017/08/06/08/46/food-2590385_640.jpg';
  img2: string = 'https://cdn.pixabay.com/photo/2017/08/12/18/59/snack-2635035_640.jpg';
  img3: string = 'https://cdn.pixabay.com/photo/2018/05/30/19/18/burger-3442227_640.jpg';
  img4: string = 'https://cdn.pixabay.com/photo/2019/01/29/18/05/burger-3962996_640.jpg';
  img:string[] = [this.img1, this.img2, this.img3, this.img4]
  show_img: string;
  RestaurantApiClient: RestaurantApiClient;
  constructor() {
    this.destinos = [];
  }

  ngOnInit():void {
  }
  guardar(nombre:string, url:string):boolean {
    if(url == "combo1")this.show_img = this.img1;
    else if(url == "combo2")this.show_img = this.img2;
    else if(url == "combo3")this.show_img = this.img3;
    else this.show_img = this.img4
    this.destinos.push(new Restaurant(nombre, url, this.show_img));
    return false;
  }

  elegido(d: Restaurant) {
    this.destinos.forEach(function(x) {x.setSelected(false);});
    d.setSelected(true);
  }

  elegir(d: Restaurant) {
    this.RestaurantApiClient.elegir(d);
  }

}
