import { Component, OnInit, Input, HostBinding, EventEmitter, Output} from '@angular/core';
import { Restaurant } from '../models/restaurant.model';
import { RestaurantApiClient } from '../models/restaurant-api-client.model';


@Component({
  selector: 'app-restaurant',
  templateUrl: './restaurant.component.html',
  styleUrls: ['./restaurant.component.css']
})
export class RestaurantComponent implements OnInit {
  @Input() destino: Restaurant;
  @Input('idx') position: number;
  @HostBinding('attr.class') cssClass = 'col-md-4';
  @Output() clicked: EventEmitter<Restaurant>;
  updates: string[];
  constructor() {
    this.clicked = new EventEmitter();
  }

  ngOnInit(): void {
  }

  ir(){
    this.clicked.emit(this.destino);
    return false;
  }

}
