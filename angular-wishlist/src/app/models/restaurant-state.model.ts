import { HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Restaurant } from '../models/restaurant.model';
import { RestaurantApiClient } from '../models/restaurant-api-client.model'

//ESTADO
export interface RestaurantState{
    items: Restaurant[];
    loading: boolean;
    favorito: Restaurant;
}

export const initializeRestaurantState = function(){
    return {
	    items: [],
	    loading: false,
	    favorito: null
    }
}

//ACCIONES
export enum RestaurantActionTypes {
  NUEVO_DESTINO = '[Destinos Viajes] Nuevo',
  ELEGIDO_FAVORITO = '[Destinos Viajes] Favorito'
}

export class NuevoDestinoAction implements Action {
  type = RestaurantActionTypes.NUEVO_DESTINO;
  constructor(public destino: Restaurant) {}
}

export class ElegidoFavoritoAction implements Action {
  type = RestaurantActionTypes.ELEGIDO_FAVORITO;
  constructor(public destino: Restaurant) {}
}

export type RestaurantActions = NuevoDestinoAction | ElegidoFavoritoAction;

//REDUCERS
export function reducerRestaurant(
	state:RestaurantState,
	action:RestaurantActions
) : RestaurantState {
	switch (action.type) {
		case RestaurantActionTypes.NUEVO_DESTINO: {
		  return {
		  		...state,
		  		items: [...state.items, (action as NuevoDestinoAction).destino ]
		  	};
		}
		case RestaurantActionTypes.ELEGIDO_FAVORITO: {
		    state.items.forEach(x => x.setSelected(false));
		    let fav:Restaurant = (action as ElegidoFavoritoAction).destino;
		    fav.setSelected(true);
		    return {
		    	...state,
		  		favorito: fav
		    };
		}
	}
	return state;
}

//EFFECTS
@Injectable()
export class RestaurantEffects {
  /*@Effect()
  nuevoAgregado$: Observable<Action> = this.actions$.pipe(
    ofType(RestaurantActionTypes.NUEVO_DESTINO),
  	map((action:NuevoDestinoAction) => new ElegidoFavoritoAction(action.destino))
  );

  constructor(private actions$: Actions) {}*/
}
