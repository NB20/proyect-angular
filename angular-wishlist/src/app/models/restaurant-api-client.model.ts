import { Restaurant } from './restaurant.model';
import { Subject,BehaviorSubject, PartialObserver } from 'rxjs';


export class RestaurantApiClient {
	destinos: Restaurant[];
  //current: Subject<Restaurant> = new BehaviorSubject<Restaurant>(null);
	constructor() {
       this.destinos = [];
	}
	add(d: Restaurant){
	  this.destinos.push(d);
	}
	getAll():Restaurant[]{
	  return this.destinos;
    }
  getById(id: string): Restaurant{
    return this.destinos.filter(function(d){return d.id.toString() === id;})[0];
  }
  elegir(d: Restaurant){
    this.destinos.forEach(x => x.setSelected(false));
    d.setSelected(true);
    //this.current.next(d);
  }
  subscribeOnChange(fn:any){
    //this.current.subscribe(fn);
  }
}
