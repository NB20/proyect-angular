export class Restaurant {
  private selected: boolean;
  public id:number = 0;
  public menu: string[];

  constructor(public nombre: string, public u: string, public img:string) {
    this.id = ++this.id;
    if(u == "combo1")this.menu = ['BigMac', 'Ensalada', "Vaso Mediano"]
    else if(u == "combo2")this.menu = ['Cuarto de Libra', "Papas Kid", "Vaso Pequeño"];
    else if(u == "combo3")this.menu = ['Pampa Burguer', "Papas Medianas", "Vaso Mediano"];
    else this.menu = ['Triple Cheese Burguer', "Papas Grandes", 'Vaso Grande'];
  }
  isSelected(): boolean{
    return this.selected;
  }
  setSelected(s: boolean){
    this.selected = s;
  }
}
