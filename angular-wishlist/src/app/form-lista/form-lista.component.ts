import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {Restaurant} from "../models/restaurant.model";
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-form-lista',
  templateUrl: './form-lista.component.html',
  styleUrls: ['./form-lista.component.css']
})
export class FormListaComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<Restaurant>;
  fg: FormGroup;
  constructor(fb: FormBuilder ) {
    this.onItemAdded = new EventEmitter();
    this.fg = fb.group({
      nombre: [''],
      url: [''],
    });
  }

  ngOnInit(): void {
  }
  guardar(nombre:string, url:string):boolean {
    let d = new Restaurant(nombre, url, "");
    this.onItemAdded.emit(d);
    return false
  }
}
