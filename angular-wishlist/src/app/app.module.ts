import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { StoreFeatureModule as NgRxStoreModule,ActionReducerMap  } from '@ngrx/store'
import { EffectsModule  } from '@ngrx/effects'


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RestaurantComponent } from './restaurant/restaurant.component';
import { ListaComponent } from './lista/lista.component';
import { PedidoDetalleComponent } from './pedido-detalle/pedido-detalle.component';
import { CarteleraComponent } from './cartelera/cartelera.component';
import { FormListaComponent } from './form-lista/form-lista.component';
import { reducerRestaurant, RestaurantState, RestaurantEffects  } from '../app/models/restaurant-state.model';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full'},
  { path: 'home', component: ListaComponent},
  {path: 'pedido', component: PedidoDetalleComponent}
];

//redux init
export interface AppState{
  destinos: RestaurantState;
}

/*const reducers: ActionReducerMap<AppState> = {
  destinos: reducerRestaurant
}*/

let reducersInitialState: AppState = {
  destinos: initializeRestaurantState()
}

//redux fin

@NgModule({
  declarations: [
    AppComponent,
    RestaurantComponent,
    ListaComponent,
    PedidoDetalleComponent,
    CarteleraComponent,
    FormListaComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    RouterModule.forRoot(routes),


  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
function initializeRestaurantState(): RestaurantState {
  throw new Error('Function not implemented.');
}

